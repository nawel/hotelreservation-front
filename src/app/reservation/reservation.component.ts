import { Component, Inject, OnInit } from '@angular/core';
import { MomentDateAdapter,MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AuthenticationService } from '../services/authentication.service';
import { ReservationsService } from '../services/reservations.service';


export const MY_FORMATS = {
  parse: {
      dateInput: 'LL'
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'YYYY'
  }
};

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
  
export class ReservationComponent implements OnInit {
  //date = new FormControl(moment());

  currentUsername:string
  minDate: Date;
  Room : any;
  reservation : any;
  username : string ;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: {selectedRoom : any, username : string}, private router: Router,public reservationService:ReservationsService,private authService:AuthenticationService, public dialogRef: MatDialogRef<ReservationComponent>) {
    const tdy : Date = new Date();
    this.minDate = new Date(tdy.setDate(tdy.getDate() + 1));
    this.Room = data.selectedRoom;
    this.username = data.username;
    console.log(this.Room)
    console.log("USERNAME")
    console.log(this.username)
  }


  ngOnInit(): void {
    this.currentUsername = this.authService.getUsername();
    // console.log("Modal selectedRoom")
    // console.log(this.selectedRoom)
  }


  onSubmit() {
    console.log("Test valid ")
    let data = this.reservationService.reservForm.value
    data.beginRent = moment(data.beginRent).format("DD/MM/YYYY")
    data.endRent = moment(data.endRent).format("DD/MM/YYYY")
    console.log(data)
   
     if (this.reservationService.reservForm.valid) {
      if (!this.reservationService.reservForm.get('idRent').value) {
        this.reservationService.addReservation(this.currentUsername, this.Room.idRoom, data).subscribe(
          (res) => {
            this.reservation = res;
            alert('Reservation enregistrée');
            console.log(this.reservation);
            this.onClose()
          },
          (err) => {
            console.log(err);
            alert("vous n'avez pas bien rempli les champs");
          }
        );
      }else{
        console.log("UPDATING RESERVATION")
        this.reservationService.updateReservation(this.reservationService.reservForm.get('idRent').value, this.reservationService.reservForm.value).subscribe(
          (res) => {
            this.reservation = res;
            console.log(this.reservation)
            alert('Votre réservation a éte mise à jour');
            console.log(this.reservation);
            this.onClose()

            //this.router.navigate(['/']);
          },
          (err) => {
            console.log(err);
            alert("vous n'avez pas bien rempli les champs");
          }
        );
      }
    }
  

   }
   
   onClose() {
    this.reservationService.reservForm.reset();
    this.reservationService.initializeFormGroup();
    this.dialogRef.close();
    location.reload() 
  }

  redirectLoginPage(){
    this.router.navigateByUrl("/login")
    .then(() => {
      window.location.reload();
    })
  }

  isAdmin(){ return this.authService.isAdmin()}
  isUser(){ return this.authService.isUser()} 
  isAuthenticated(){ return this.authService.isAuthenticated()}

}

