import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { IRegistration } from '../interfaces/interface';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})





export class RegisterComponent implements OnInit {
  myModel = {
    idUser: '',
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    phone: null,
    mail: '',
    isAdmin:false,
  } as IRegistration
  user:any;
  constructor(private authService:AuthenticationService, private router:Router) { }

  ngOnInit(): void {
  }

  onRegister(data){
    console.log("DATA TO POST : " )
    console.log(data)
      this.authService.register(data,data.isAdmin)
        .subscribe(resp=>{
          console.log(resp);
          this.user=resp;
          this.router.navigateByUrl("/login");
          
        }, err=>{
          console.log(err);
          alert("vous n'avez pas bien rempli les champs");
        })
       }

}
