import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservationComponent } from './reservation/reservation.component';

import { RoomsComponent } from './rooms/rooms.component';
import { UsersComponent } from './users/users.component';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { AuthenticationService } from './services/authentication.service';
import { MyProfilComponent } from './my-profil/my-profil.component';
import { SuiteComponent } from './suite/suite.component';
import { VenueComponent } from './venue/venue.component';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {
    path:"users", component:UsersComponent
  },
  {
    path:"", component:RoomsComponent
  },
  {
    path:"suite", component:SuiteComponent
  },
  {
    path:"venue", component:VenueComponent
  },
  {
    path:"reservations", component:ReservationListComponent
  },
  {
    path:"reservation", component:ReservationComponent
  },
  {
    path:"login", component:AuthenticateComponent
  },
  {
    path:"register", component:RegisterComponent
  },
  {
    path:"my_profil", component:MyProfilComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

  title = 'HotelAppli';

  constructor(private authService:AuthenticationService){}


  isAdmin(){
    return this.authService.isAdmin();
  }

  isUSer(){
    return this.authService.isUser();
  }

  
}
