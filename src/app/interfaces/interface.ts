export interface ISuite{
        "idRoom": number,
        "roomNumber": number,
        "name": string,
        "description": string,
        "floor": number,
        "price": number,
        "nbRoom": number,
        "hasKitchen": boolean
}

export interface IVenue{
    "idRoom": number,
    "roomNumber": number,
    "name": string,
    "description": string,
    "floor": number,
    "price": number,
    "nbPerson": number,
    "setup": string,
    "withPodium": boolean
}


export interface IRegistration{
    "idUser": string,
    "username": string,
    "password": string,
    "firstName": string,
    "lastName": string,
    "phone": string,
    "mail": string,
    "isAdmin" : boolean
}