import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'appliHotel-front';
  username:string;


  constructor(private authService:AuthenticationService, private router: Router) { }
  ngOnInit(): void {
    this.authService.loadToken();
    this.username = this.authService.getUsername()
    console.log("USERNAME")
    console.log(this.username)
    
    //console.log("NAAANAAA");
    //console.log(this.authService.getUsername());

  }

  isAdmin(){ 
    return this.authService.isAdmin()
  }
  isUser(){ 
    return this.authService.isUser()
  } 
  isAuthenticated(){ 
    return this.authService.isAuthenticated()
  }

  logOut(){
    this.authService.logOut();
    location.reload()
  }

}
