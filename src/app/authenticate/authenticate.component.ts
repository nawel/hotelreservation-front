import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.css']
})
export class AuthenticateComponent implements OnInit {

  constructor(private authService:AuthenticationService, private router:Router) { }


  ngOnInit(): void {
  }


  onLogin(data){
    console.log("DATA TO POST : " )
    console.log(data)
    this.authService.login(data)
    .subscribe(resp=>{
      console.log(resp);
      console.log(resp.headers.get('Authorization'));
      let jwt = resp.headers.get('Authorization');
      this.authService.saveToken(jwt);
      this.router.navigateByUrl("/")
      .then(() => {
        window.location.reload();
      });
    }, err=>{
      console.log(err);
      alert("username ou mot de passe incorrect");
    })
  }

}
