import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ReservationComponent } from '../reservation/reservation.component';
import { AuthenticationService } from '../services/authentication.service';
import { ReservationsService } from '../services/reservations.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {
  reservations: any;
  myReservations: any;
  panelOpenState = false;

  constructor(private reservationService:ReservationsService, private authService:AuthenticationService,private dialog: MatDialog, private router:Router) { }

  ngOnInit(): void {

    if (!this.authService.isAuthenticated()){
      this.router.navigate(['/']);
    }
    this.panelOpenState = true;

    this.getAllReservations();
    this.getReservationsByUsername();
  }
  getAllReservations(){
    this.reservationService.getAllReservations()
    .subscribe(data=>{
      this.reservations =data;
      console.log(data);
      console.log("ok");
    }, err=>{
      console.log(err);
    })
  }
  getReservationsByUsername(){
    this.reservationService.getReservationsByUsername(this.authService.getUsername())
    .subscribe(data=>{
      this.myReservations =data;
      console.log(data);
      console.log("ok");
    }, err=>{
      console.log(err);
    })
  }

  onEdit(reservation){
    console.log("ON EDIT")
    console.log(reservation)
    //this.username = reservation.person.username
    //this.resComponent.username = reservation.person.username

   console.log(reservation.person.username)

    this.reservationService.populateForm(reservation);
    this.dialog.open(ReservationComponent,{data:{selectedRoom: reservation.room, username : reservation.person.username}})   
}

  
  onDelete(id:number):void{
    let c = confirm("Êtes-vous sûr de vouloir annuler cette réservation ?");
    if (!c) return;
    this.reservationService.deleteReservation(id)
    .subscribe(data=>{
      this.getAllReservations()
      this.getReservationsByUsername()
    },err=>{
      console.log(err);
    })
  }


  isAdmin(){ return this.authService.isAdmin()}
  isUser(){ return this.authService.isUser()} 
  isAuthenticated(){ return this.authService.isAuthenticated()}
}
