import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profil',
  templateUrl: './my-profil.component.html',
  styleUrls: ['./my-profil.component.css']
})
export class MyProfilComponent implements OnInit {

  user:any;
  username:string

  constructor(private userService:UserService, private authService:AuthenticationService, private router: Router) { 
    
  }

  ngOnInit(): void {
    this.username = this.authService.getUsername();
    if (this.authService.isAuthenticated()) {
      this.userService.getUserByUserName(this.username)
      .subscribe(data=>{
        console.log(data);
        this.user =data;
        console.log("ok");
      }, err=>{
        console.log(err);
    })
    } 
    else {
      this.router.navigate(['/']);
    }    
      
   
  }

}
