import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.css']
})
export class VenueComponent implements OnInit {


  type: string;

  venue:any;
  
  dispositions = [{label : "fête" , value : "PARTY"},{label : 'Conférence', value : 'CONFERENCE'}];
  selectedDispo="";
  constructor(public roomsService:RoomsService, private router: Router, private authService:AuthenticationService, private dialogRef: MatDialogRef<VenueComponent>) { 
  }

  ngOnInit(): void {
    if (!this.authService.isAdmin())
      this.router.navigate(['/']);
  }
  
 
  selectedDisposition(event: MatSelectChange) {
    this.selectedDispo = event.value;
    console.log(this.selectedDispo);
  }
  onSubmit() {
    console.log("Test valid ")
    console.log(this.roomsService.venueForm)

    console.log(this.roomsService.venueForm.valid)
    if (this.roomsService.venueForm.valid) {
      if (!this.roomsService.venueForm.get('idRoom').value) {
        this.roomsService.addVenue(this.roomsService.venueForm.value).subscribe(
          (res) => {
            this.venue = res;
            alert("Venue enregistrée");
            console.log(this.venue);
            this.onClose()
            //this.router.navigate(['/']);
          },
          (err) => {
            console.log(err);
            alert("vous n'avez pas bien rempli les champs du formulaire");
          }
        );
      }else{
        this.roomsService.updateVenue(this.roomsService.venueForm.get('idRoom').value, this.roomsService.venueForm.value).subscribe(
          (res) => {
            this.venue = res;
            alert('Mise à jour de venue réussie');
            console.log(this.venue);
            this.onClose()
            //this.router.navigate(['/']);
          },
          (err) => {
            console.log(err);
          }
        );
      }
    }
  }

  onClose() {
    this.roomsService.venueForm.reset();
    this.roomsService.initializeFormGroup("venue");
    this.dialogRef.close();
    location.reload()
  }
}
