import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { MaterialModule } from './MaterialModule';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RoomsComponent } from './rooms/rooms.component';
import { UsersComponent } from './users/users.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { MyProfilComponent } from './my-profil/my-profil.component';
import { SuiteComponent } from './suite/suite.component';
import { VenueComponent } from './venue/venue.component';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { ReservationComponent } from './reservation/reservation.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomsComponent,
    UsersComponent,
    ReservationComponent,
    AuthenticateComponent,
    MyProfilComponent,
    SuiteComponent,
    VenueComponent,
    ReservationListComponent,
    ReservationComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    MaterialModule,
    FormsModule,
    BrowserAnimationsModule, ReactiveFormsModule
  ],
  providers: [ReservationComponent],
  bootstrap: [AppComponent],
  entryComponents: [SuiteComponent,VenueComponent]
})
export class AppModule { }
