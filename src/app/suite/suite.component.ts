import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
//import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-suite',
  templateUrl: './suite.component.html',
  styleUrls: ['./suite.component.css'],
})
export class SuiteComponent implements OnInit {
  etages = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
  ];
  suite: any;

  constructor(
    public roomsService: RoomsService,
    private router: Router,
    private authService: AuthenticationService,
    private dialogRef: MatDialogRef<SuiteComponent>
  ) {}

  ngOnInit(): void {
    if (!this.authService.isAdmin()) this.router.navigate(['/']);
  }

  onSubmit() {
    console.log(this.roomsService.suiteForm.get('idRoom').value)
    console.log("Test valid ")
    console.log(this.roomsService.suiteForm.valid)
    if (this.roomsService.suiteForm.valid) {
      if (!this.roomsService.suiteForm.get('idRoom').value) {
        this.roomsService.addSuite(this.roomsService.suiteForm.value).subscribe(
          (res) => {
            this.suite = res;
            alert('Suite enregistrée');
            console.log(this.suite);
            this.onClose()
            //this.router.navigate(['/']);
          },
          (err) => {
            alert("suite enregistrée");
            console.log(this.suite);
            this.onClose()
          }
        );
      } else{
        this.roomsService.updateSuite(this.roomsService.suiteForm.get('idRoom').value,this.roomsService.suiteForm.value).subscribe(
          (res) => {
            this.suite = res;
            alert('Mise à jour de suite réussie');
            console.log(this.suite);
            this.onClose()
            //this.router.navigate(['/']);
          },
          (err) => {
            console.log(err);
          }
        );
      }
    }
  }
  onClose() {
    this.roomsService.suiteForm.reset();
    this.roomsService.initializeFormGroup("suite");
    this.dialogRef.close();
    location.reload()
  }
}


