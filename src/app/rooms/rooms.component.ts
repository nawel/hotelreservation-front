import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../services/rooms.service';
import { AuthenticationService } from '../services/authentication.service';
import {MatDialog} from '@angular/material/dialog';

import { SuiteComponent } from '../suite/suite.component';
import { VenueComponent } from '../venue/venue.component';
import { ReservationsService } from '../services/reservations.service';
import { ReservationComponent } from '../reservation/reservation.component';
@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  rooms:any;
  suites:any;
  venues: any;

  constructor(private roomsService:RoomsService,private reservationService:ReservationsService, private authService:AuthenticationService, private dialog: MatDialog) { }

  ngOnInit(): void {
    // this.roomsService.getAllRoom()
    // .subscribe(data=>{
    //   this.rooms =data;
    //   console.log(data);
    //   console.log("ok");
    // }, err=>{
    //   console.log(err);
    // })
   
    this.getAllRooms()
  }

  getAllRooms(){
    this.roomsService.getAllSuite()
    .subscribe(data=>{
      this.suites =data;
      console.log(data);
      console.log("ok");
    }, err=>{
      console.log(err);
    })
    this.roomsService.getAllVenue()
    .subscribe(data=>{
      this.venues =data;
      console.log(data);
      console.log("ok");
    }, err=>{
      console.log(err);
    })
  }

  onCreate(type:string){
      this.roomsService.initializeFormGroup(type);
      if (type == "suite"){
        this.dialog.open(SuiteComponent);
      } else if (type=="venue"){
        this.dialog.open(VenueComponent)
      }
  }
  onEdit(type : string, room){
    this.roomsService.populateForm(type,room);
      if (type == "suite"){
        this.dialog.open(SuiteComponent);
      } else if (type=="venue"){
        this.dialog.open(VenueComponent)
      }
}
  
  onDelete(id:number):void{
    let c = confirm("Êtes-vous sûr de vouloir supprimer cette chambre ?");
    if (!c) return;
    this.roomsService.deleteRoom(id)
    .subscribe(data=>{
      this.getAllRooms()
    },err=>{
      alert("Vous ne pouvez pas supprimer cette chambre, elle est réservée ")
      console.log(err);
    })
  }

  onReserver(room){
    this.reservationService.initializeFormGroup();
    console.log("OnReserver")
    console.log(room);
    this.dialog.open(ReservationComponent,{data:{selectedRoom: room, username : this.authService.getUsername()}});
  }

  isAdmin(){ return this.authService.isAdmin()}
  isUser(){ return this.authService.isUser()} 
  isAuthenticated(){ return this.authService.isAuthenticated()}
}
