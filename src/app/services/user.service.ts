import { Injectable } from '@angular/core';
import { UsersComponent } from '../users/users.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient:HttpClient, private authService:AuthenticationService) { }

  public getAllUser()  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.get(environment.apiURL+"/user", {headers:headers});
  }

  public getUserByUserName(username:string)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.get(environment.apiURL+"/user/profil/"+username, {headers:headers});
  }

  
}
