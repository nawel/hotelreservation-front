import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  jwt:string;
  username:string;
  roles:string[];

  constructor(private http:HttpClient) { }


  login(data){
    return this.http.post(environment.apiURL+"/login", data, {observe:'response'})
  }

 

  register(data,isAdmin){
    if(isAdmin == true){
      return this.http.post(environment.apiURL+"/register/admin", data, {observe:'response'})
    } else if (isAdmin == false){
      return this.http.post(environment.apiURL+"/register", data, {observe:'response'})
    }
  }
 
  saveToken (jwt: string){
    localStorage.setItem('token', jwt);
    this.jwt=jwt;
    this.parseJWT();

  }

  parseJWT(){
    let jwtHelper = new JwtHelperService();
    let objJWT = jwtHelper.decodeToken(this.jwt);
    this.username=objJWT.sub;
    this.roles=objJWT.roles;
  }

  isAdmin(){
    if (this.roles != undefined)
      return this.roles.indexOf('ADMIN')>=0;
  }

  isUser(){
    if (this.roles != undefined)
      return this.roles.indexOf('USER')>=0;
  }

  isAuthenticated(){
    
    return this.roles && (this.isAdmin() || this.isUser());
  }

  loadToken(){
    this.jwt=localStorage.getItem('token');
    this.parseJWT();
  }

  getUsername(){
    if (this.username != undefined)
    return this.username;
  }

  logOut(){
    localStorage.removeItem('token');
    this.initCredentials();
  }

  initCredentials(){
    this.jwt=undefined;
    this.username=undefined;
    this.roles=undefined;
  }

}
