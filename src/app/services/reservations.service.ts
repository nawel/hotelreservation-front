import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReservationComponent } from '../reservation/reservation.component';
import { AuthenticationService } from './authentication.service';
import { UserService } from './user.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  constructor(private httpClient:HttpClient, private dialog: MatDialog,private authService:AuthenticationService, private userService:UserService) { }

  public getAllReservations()  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.get(environment.apiURL+"/reservation", {headers:headers});
  }
  public getReservationsByUsername(username:string)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.get(environment.apiURL+"/reservation/"+username, {headers:headers});
  }
  public deleteReservation(id : number){
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.delete(environment.apiURL+"/reservation/"+id, {headers:headers});
  }

  public addReservation(username:string, idRoom:number, reservation:any)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.post(environment.apiURL+"/reservation/"+ username + "/" + idRoom, reservation, {headers:headers});
  }
  public updateReservation(id : number,reservation: any)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.put(environment.apiURL+"/reservation/"+id, reservation, {headers:headers});
  }


  reservForm: FormGroup = new FormGroup({
    idRent: new FormControl(null),
    beginRent: new FormControl('', Validators.required),
    endRent: new FormControl('', Validators.required),
  });
  initializeFormGroup() {
      this.reservForm.setValue({
        idRent: null,
        beginRent: '',
        endRent: ''
      });
    }
  populateForm(reservation : any) {
    console.log("POPULATE");
    console.log(reservation.beginRent);
    console.log(this.reservForm.value['idRent']);
    let data = { "idRent": reservation.idRent ,"beginRent": reservation.beginRent , "endRent" : reservation.endRent};
    console.log(data)
    this.reservForm.setValue(data);
  }



}
