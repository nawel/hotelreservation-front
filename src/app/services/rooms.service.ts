import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ISuite,IVenue } from '../interfaces/interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from './authentication.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  

  constructor(private httpClient:HttpClient, private authService:AuthenticationService) { }
  
  public getAllRoom()  {
    return this.httpClient.get(environment.apiURL +"/rooms");
  }
  public deleteRoom(id : number){
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.delete(environment.apiURL+"/room/"+id, {headers:headers});
  }
  public getAllSuite()  {
    return this.httpClient.get(environment.apiURL+"/room/suite");
  }
  public getAllVenue()  {
    return this.httpClient.get(environment.apiURL+"/room/venue");
  }

  public addSuite(suite: ISuite)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.post(environment.apiURL+"/room/suite", suite, {headers:headers});
  }

  public addVenue(venue: IVenue)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.post(environment.apiURL+"/room/venue",venue, {headers:headers});
  }
  public updateSuite(id : number,suite: ISuite)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.put(environment.apiURL+"/room/suite/"+id, suite, {headers:headers});
  }

  public updateVenue(id : number,venue: IVenue)  {
    let headers = new HttpHeaders({'authorization': this.authService.jwt})
    return this.httpClient.put(environment.apiURL+"/room/venue/"+id,venue, {headers:headers});
  }


  suiteForm: FormGroup = new FormGroup({
    idRoom: new FormControl(null),
    roomNumber: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    floor: new FormControl('', Validators.required),
    price: new FormControl('', [Validators.required]),
    nbRoom: new FormControl('', Validators.required),
    hasKitchen: new FormControl(false)
  });
  venueForm: FormGroup = new FormGroup({
    idRoom: new FormControl(null),
    roomNumber: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    floor: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    nbPerson: new FormControl('', Validators.required),
    setup: new FormControl('', Validators.required),
    withPodium: new FormControl(false)
  });
  
  initializeFormGroup(type :string) {
      if(type == "suite"){
        this.suiteForm.setValue({
          idRoom: null,
          roomNumber: '',
          name: '',
          description:'' ,
          floor:'',
          price: '',
          nbRoom: '',
          hasKitchen:false
        });
      } else if (type == "venue"){
        this.venueForm.setValue({
          idRoom: null,
          roomNumber: '',
          name: '',
          description:'' ,
          floor:'',
          price: '',
          nbPerson: '',
          setup:'',
          withPodium:false
        });
      } 
  }

  populateForm(type,room) {
    if (type == "suite"){
      this.suiteForm.setValue(room);
    }else if (type == "venue"){
      this.venueForm.setValue(room);
    }
  }




}
