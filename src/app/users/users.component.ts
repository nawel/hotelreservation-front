import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users:any;

  constructor(private userService:UserService, private authService:AuthenticationService, private router: Router) { }

  ngOnInit(): void {

    if (this.authService.isAdmin()) {
      this.userService.getAllUser()
      .subscribe(data=>{
        this.users =data;
        console.log(data);
        console.log("ok");
      }, err=>{
        console.log(err);
    })
    } 
    else {
      this.router.navigate(['/']);
    }

      

  }

}
